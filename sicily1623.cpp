//题目网址:
//http://soj.me/1623

//题目分析:
//lcm = a*b / gcd(a, b), gcd是a,b的最大公约数

#include <iostream>

using namespace std;

int gcd(int m, int n)
{
    while (n != 0) {
        int rem = m % n;
        m = n;
        n = rem;
    }
    return m;
}

int main()
{
    int N, num = 1;
    cin >> N;
    int a, b;
    
    while (num <= N) {
        cin >> a >> b;
        cout << num << ' ' << a*b/gcd(a, b) << ' ' << gcd(a, b) << endl;
        num++;
    }
    
    return 0;
}                                 